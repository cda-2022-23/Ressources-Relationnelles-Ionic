import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { baseUrl } from '../constants';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage {

  ressources = [];
  timer = undefined;

  constructor(private http: HttpClient, private router: Router) { }



  search(searchText: any) {
    clearTimeout(this.timer);
    console.log(searchText.detail.value.length);
    if (searchText.detail.value.length > 1) {
      setTimeout(() => {
        let params = new HttpParams().set('search', searchText.detail.value);
        this.http.get(baseUrl + "/searchRessource",
          { params: params }).subscribe((response: any) => {
            this.ressources = response;
          })
      }, 600)
    }
  }

  getRessourceDetail(ressource: any) {
    this.router.navigate(['/ressource-details'], { queryParams: { 'id': ressource['id'] } })
  }

}
