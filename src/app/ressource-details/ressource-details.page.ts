import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { baseUrl } from '../constants';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-ressource-details',
  templateUrl: './ressource-details.page.html',
  styleUrls: ['./ressource-details.page.scss'],
})
export class RessourceDetailsPage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private storage: Storage
  ) {}

  ressource: any = {};

  user: any = {};

  liked: boolean = false;

  ngOnInit() {
    let id: any;
    this.route.queryParamMap.subscribe((params) => {
      id = params;
      id = id.params.id;
    });
    this.storage.get('user').then((myUser) => {
      this.user = myUser;
      if (this.user['likes'].includes(id)) {
        this.liked = true;
      }
      let params = new HttpParams()
        .set('ressourceId', id)
        .set('userId', myUser.id);
      this.http
        .get(baseUrl + '/Ressource', { params: params })
        .subscribe((response: any) => {
          this.ressource = response;
          console.log(this.ressource);
          this.ressource.contenu = this.sanitizer.bypassSecurityTrustHtml(
            this.ressource.contenu
          );
        });
    });
  }

  ngOnDestroy() {
    console.log(this.user);
    let formData: FormData = new FormData();
    formData.append('ressourceId', this.ressource.id);
    formData.append('userId', this.user.id);
    formData.append('liked', this.liked.toString());
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };
    this.http
      .post(baseUrl + '/setLike', formData, httpOptions)
      .subscribe(async (response: any) => {});
  }

  like(liked: boolean) {
    if (liked) {
      this.user.likes.push(this.ressource.id);
      this.storage.set('user', this.user);
      this.liked = true;
    } else {
      const index = this.user.likes.indexOf(this.ressource.id);
      if (index > -1) {
        this.user.likes.splice(index, 1);
        this.storage.set('user', this.user);
        this.liked = false;
      }
    }
    console.log(this.user.likes);
  }
}
