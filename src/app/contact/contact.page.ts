import { Component, OnInit } from '@angular/core';
import { baseUrl } from '../constants';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})

export class ContactPage {


  registerForm = this.fb.group({
    name: ['', Validators.required],
    surname: ['', Validators.required],
    phoneNumber: ['', Validators.required],
    email: ['', Validators.required],
    reason: ['', Validators.required],
  })

  nameRequired = false;
  surnameRequired = false;
  phoneNumberRequired = false;
  emailRequired = false;
  reasonRequired = false;


  constructor(private fb: FormBuilder, private http: HttpClient) { }

  onSubmit() {
    if (this.errorControl.name.errors == null && this.errorControl.surname.errors == null && this.errorControl.phoneNumber.errors == null && this.errorControl.email.errors == null && this.errorControl.reason.errors == null) {
      let formData: FormData = new FormData();
      let myData: any = this.registerForm.value;


      formData.append('name', myData.name);
      formData.append('surname', myData.surname);
      formData.append('phoneNumber', myData.phoneNumber);
      formData.append('email', myData.email);
      formData.append('reason', myData.reason);
      const httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
        })
      }
      this.http.post(baseUrl + "/SendSupportMail", formData, httpOptions).subscribe((response: any) => {
        console.log(response.code);
        switch (response.code) {
          case '0001':
            alert('Message envoyé !');
            break;
          case '0099':

            alert('Une erreur s\'est produite, veuillez réessayer plus tard.');
            break;
        }
      })
    } else {
      if (this.errorControl.name.errors != null) {
        this.nameRequired = true;
      }
      if (this.errorControl.surname.errors != null) {
        this.surnameRequired = true;
      }
      if (this.errorControl.phoneNumber.errors != null) {
        this.phoneNumberRequired = true;
      }
      if (this.errorControl.email.errors != null) {
        this.emailRequired = true;
      }
      if (this.errorControl.reason.errors != null) {
        this.reasonRequired = true;
      }
    }
  }

  get errorControl() {
    return this.registerForm.controls;
  }


}
