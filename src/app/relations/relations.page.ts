import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseUrl } from '../constants';

@Component({
  selector: 'app-relations',
  templateUrl: './relations.page.html',
  styleUrls: ['./relations.page.scss'],
})
export class RelationsPage {


  registerForm = this.fb.group({
    alias: ['', Validators.required],
  })

  aliasRequired = false;



  constructor(private fb: FormBuilder, private http: HttpClient) { }



  onSubmit() {
    if (this.errorControl.alias == null) {
      let formData: FormData = new FormData();
      let myAlias: any = this.registerForm.value;
      formData.append('alias', myAlias.alias);
      const httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
        })
      }
      this.http.post(baseUrl + "/InviteUser", formData, httpOptions).subscribe((response: any) => {
        console.log(response.code);
        switch (response.code) {
          case '0001':
            break;
          case '0099':

            alert('Une erreur s\'est produite, veuillez réessayer plus tard.');
            //this.router.navigate(['/tabs/']);
            break;
        }
      })

    }
  }


  get errorControl() {
    return this.registerForm.controls;
  }

}
